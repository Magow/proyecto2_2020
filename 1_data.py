#  !/usr/bin/env python3
#   -*- coding:utf-8 -*-

from os import system
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np



MAIN_DATA = "suministro_alimentos_kcal.csv"

def open():

    df_s = pd.read_csv(MAIN_DATA)
    #print(df_suministros)
    #print(df_suministros.columns)
    #print(f"Las dimensiones del dataset son {df_s.shape}")
    #print(f"Las columnas de mi dataset son {df_s.columns}")
    df_s["Death_count_COVID19"] = (df_s["Deaths"] * df_s["Population"])/100
    df_s["Cases_count_COVID19"] = (df_s["Confirmed"] * df_s["Population"])/100

    return df_s


def filter_data(first_data):

    first_data.drop(["Alcoholic Beverages",
                    "Animal Products",
                    "Aquatic Products, Other",
                    "Eggs",
                    "Fish, Seafood",
                    "Fruits - Excluding Wine",
                    "Meat",
                    "Milk - Excluding Butter",
                    "Miscellaneous",
                    "Offals",
                    "Oilcrops",
                    "Pulses",
                    "Spices",
                    "Stimulants",
                    "Sugar Crops",
                    "Treenuts",
                    "Vegetal Products",
                    "Vegetables",
                    "Recovered",
                    "Active",
                    "Unit (all except Population)"],
                    axis=1, inplace = True)

    return first_data


def menu():

    print("Menú de acciones para base experimental de datos proteínas")
    print('\n')
    print("(1) Hipotesis 1")
    print("(2) Hipotesis 2")
    print("(3) Hipotesis 3")
    print("(4) Hipotesis 4")
    print("(5) Salir del análisis de datos")


if __name__ == '__main__':

    system('clear')
    first_data = open()
    df_hyp = filter_data(first_data)
    #print(df_hyp.columns)
    #print(helpful_data_hypothesis)
    #print(f"Las dimensiones del dataset2 son {helpful_data_hypothesis.shape}")

    #for index, row in df_hyp1.iterrows():
    #    print(index, row["Country"])

    #print(df_hyp.sort_values('Country', ascending=False))
    #print(df_hyp.head(2))




    while True:

        menu()
        print('\n')
        choice = input("Ingrese la acción que desee realizar: ")

        if choice == "1":
            system('clear')
            print("Hipotesis 1")
            print("¿Es directamente proporcional la ingesta de ", end=" ")
            print("grasas animales y las muertes de COVID19?")

            df_sorted1 = df_hyp.sort_values(by = ["Cases_count_COVID19","Animal fats"],
                    ascending=False)[["Country", "Animal fats", "Cases_count_COVID19"]]
            print(df_sorted1[0:16])

            print("Según lo presentado en la muestra la falta de ", end= " ")
            print("mas datos para concluir deja la hipótesis ambigua")


            #plt.title("Relación grasas/covid")
            #plt.xlabel("grasas kcal")
            #plt.ylabel("paises")
            #plt.legend()
            #plt.show()

        elif choice == "2":
            system('clear')
            print("Hipotesis 2")
            print("¿Los porcentajes de ingesta de aceites vegetales ", end=" ")
            print("y cereales afectan a la obesidad en un país?")

            df_sorted2 = df_hyp.sort_values(by = ["Vegetable Oils","Obesity"],
                    ascending=False)[["Country", "Vegetable Oils", "Obesity"]]

            print(df_sorted2[0:16])
            print("Los porcentajes de ingesta de aceites vegetales ", end= " ")
            print("pueden relacionarse con la obesidad")
            print("Más datos deben ser analizados para identificar ", end= " ")
            print("las causas principales de la ovesidad siendo esta ya una de ellas")


        elif choice == "3":
            system('clear')
            print("Hipotesis 3")
            print("¿Es la desnutrición un factor determinante en las ", end=" ")
            print("muertes por COVID19?")

            df_sorted3 = df_hyp.sort_values(by = ["Undernourished","Death_count_COVID19"],
                    ascending=False)[["Country", "Undernourished", "Death_count_COVID19"]]
            print(df_sorted3[0:16])
            print("Por los porcentajes entregados no es posible ", end= " ")
            print("realizar una buena relación entre desnutrición y COVID19")


        elif choice == "4":
            system('clear')
            print("Hipotesis 4")
            print("¿Que país de sudamerica tiene mayor tasa de obesidad?")
            sudamerica = ["Argentina", "Bolivia", "Brasil", "Chile", "Colombia",
                "Ecuador", "Guyana", "Paraguay", "Perú","Surinam",
                "Trinidad y Tobago", "Uruguay", "Venezuela", "Guyana Francesa "]

            df_sorted4 = df_hyp.sort_values(by = ["Obesity"],
                    ascending=False)[["Country", "Starchy Roots", "Undernourished"]]
            for i in range()
            if df_sorted4["Country"] in sudamerica:
                print(df_sorted4["Country", "Obesity"])

            print("Los porcentajes de ingesta de aceites vegetales ", end= " ")
            print("pueden relacionarse con la obesidad")
            print("Más datos deben ser analizados para identificar ", end= " ")
            print("las causas principales de la ovesidad siendo esta ya una de ellas")

        elif choice == "5":
            system('clear')
            print("Ha deseado salir del programa, adios ")
            break
        else:
            system('clear')
            print("El valor ingresado no entrega ninguna respuesta", end=" ")
            print(" ingrese una opción valida")
            print('\n')
